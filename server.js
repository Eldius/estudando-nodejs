
var http = require('http');
var app = require('./config/express')(app);
require('./config/passport')(process.env.CALLBACK);
require('./config/database.js')(process.env.MONGO);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express Server escutando em: ' + process.env.IP + ":" + app.get('port'));
});

