angular.module('contatooh').controller('ContatosController',
	function($scope, Contato) {
		$scope.total = 0;

		$scope.filtro = '';

		$scope.mensagem = {texto: ''};

		$scope.incrementa = function() {
			$scope.total++;
		};

		$scope.contatos = [];

		function buscaContatos() {
			Contato.query(
				function(contatos) {
					$scope.contatos = contatos;
				},
				function(erro) {
					console.log("Não foi possível obter a lista de contatos");
					console.log(erro);
					$scope.mensagem = {
							texto: 'Não foi possível obter a lista'
						};
				}
			);
		}

		$scope.remove = function(contato) {
			Contato.delete(
				{id: contato._id},
				buscaContatos,
				function(erro) {
					$scope.mensagem = {
							texto: 'Não foi possível remover o contato'
						};
					console.log(erro);
				}
			);
		};

		buscaContatos();
	}
);
