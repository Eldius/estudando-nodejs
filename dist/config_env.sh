#!/bin/bash

echo "installing las stable node version" && \
sudo nvm install stable && \
sudo nvm use stable && \
echo "installing grunt cli" && \
sudo npm install -g grunt-cli && \
echo "installing karma cli" && \
sudo npm install -g karma-cli && \
echo "installing protractor" && \
sudo npm install -g protractor && \
echo "installing bower" && \
sudo npm install -g bower && \
echo "updating webdriver manager" && \
sudo webdriver-manager update
