
var HtmlReporter = require('protractor-html-screenshot-reporter');

exports.config = {
    specs: ['../test/e2e/**/*.js'],
    rootElement: 'html',
    framework: 'jasmine2',
    onPrepare: function() {

        var reporter=new HtmlReporter({
            baseDirectory: './tmp' // a location to store screen shots.
            , docTitle: 'Protractor Demo Reporter'
            , docName:    'index.html'
            , metaDataBuilder: function metaDataBuilder(spec, descriptions, results, capabilities) {
                // Return the description of the spec and if it has passed or not:
                return {
                    description: descriptions.join(' ')
                    , passed: results.passed()
                };
            }
            , takeScreenShotsForSkippedSpecs: true
        });
        jasmine.getEnv().addReporter(reporter);

        browser.get('http://localhost:8080');
        browser.driver.findElement(by.id('entrar')).click();
        browser.driver.findElement(by.id('login_field')).sendKeys('eldiusjr');

        browser.driver.findElement(by.id('password')).sendKeys('123Senha');

        browser.driver.findElement(by.name('commit')).click();
    }
};
