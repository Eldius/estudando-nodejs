
var passport = require('passport');
var GitHubStrategy = require('passport-github').Strategy;

var mongoose = require('mongoose');

module.exports = function(homeUrl) {

    var Usuario = mongoose.model('Usuario');

    var callbackUrl = homeUrl + '/auth/github/callback';
    //var callbackUrl = homeUrl;

    passport.use(new GitHubStrategy({
            clientID: '7dda92212823d52d795c',
            clientSecret: '3a3539dc220ac96b908c42ba5c2dc72250e40672',
            callbackURL: callbackUrl
        }, function(accessToken, refreshToken, profile, done) {
            Usuario.findOrCreate(
                { "login" : profile.username},
                { "nome" : profile.username},
                function(erro, usuario) {
                    if(erro) {
                        console.log(erro);
                        return done(erro);
                    }
                    return done(null, usuario);
                }
            );
        }
    ));

    /*
        Chamado apenas UMA vez e recebe o usuário do nosso
        banco disponibilizado pelo callback da estratégia de
        autenticação. Realizará a serialização apenas do
        ObjectId do usuário na sessão.
    */

    passport.serializeUser(function(usuario, done) {
        done(null, usuario._id);
    });

    // Recebe o ObjectId do usuário armazenado na sessão
    // Chamado a CADA requisição
    passport.deserializeUser(function(id, done) {
        Usuario.findById(id).exec()
            .then(function(usuario) {
                done(null, usuario);
            });
    });
};

